# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Luca Weiss <luca@z3ntu.xyz>
pkgname=qt5-qtpim
pkgver=0_git20181129
_commit="380605ef048372a40587f0231ba00df146bc070e"
pkgrel=0
arch="x86_64"
url='http://qt-project.org/'
license="LGPL-3.0"
pkgdesc='Qt Personal Information Management'
depends=""
depends_dev="qt5-qtdeclarative-dev"
makedepends="$depends_dev git"
source="$pkgname-$_commit.tar.gz::https://github.com/qt/${pkgname/qt5-/}/archive/$_commit.tar.gz"
subpackages="$pkgname-dev $pkgname-doc"
builddir="$srcdir/${pkgname/qt5-/}-$_commit"
options="!check"

prepare() {
	default_prepare
	# Something in the build system only runs the syncqt.pl script when it's in a git repo
	git init
}

build() {
	cd "$builddir"
	qmake-qt5
	make
}

package() {
	cd "$builddir"
	make INSTALL_ROOT="$pkgdir" install

	# Drop QMAKE_PRL_BUILD_DIR because reference the build dir
	find "$pkgdir/usr/lib" -type f -name '*.prl' \
		-exec sed -i -e '/^QMAKE_PRL_BUILD_DIR/d' {} \;

	install -d "$pkgdir"/usr/share/licenses
	ln -s /usr/share/licenses/qt5-base "$pkgdir"/usr/share/licenses/$pkgname
}
sha512sums="dadd6484b34540b5f47f39b1a09e404384b1a22d8c757a56f9ce1efbf047a1ece556fb3b1fe32dfe54c82985023776d182420f55576c9d87f1267c94b144ad77  qt5-qtpim-380605ef048372a40587f0231ba00df146bc070e.tar.gz"
