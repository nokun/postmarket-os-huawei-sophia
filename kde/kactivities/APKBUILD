# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kactivities
pkgver=5.55.0
pkgrel=0
arch="all"
pkgdesc="Core components for the KDE's Activities"
url="https://community.kde.org/Frameworks"
license="GPL-2.0 LGPL-2.1"
depends="qt5-qtbase-sqlite"
depends_dev="kcoreaddons-dev kconfig-dev kwindowsystem-dev qt5-qtdeclarative-dev kio-dev"
makedepends="$depends_dev extra-cmake-modules boost doxygen qt5-qttools-dev boost-dev"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/$pkgname-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-libs $pkgname-doc"
builddir="$srcdir/$pkgname-$pkgver/build"

prepare() {
	mkdir "$builddir"
}

build() {
	cd "$builddir"
	cmake .. \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DQML_INSTALL_DIR=lib/qt/qml \
		-DBUILD_QCH=ON
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}
sha512sums="84a48e658fcfe58c2878df27c90073b16682eedbde26311a8a3029203b69ffd3d75e2b61a870efd77a2ff0a4b520ae009e745d3ed85a60ebd723fcdf03aff61b  kactivities-5.55.0.tar.xz"
